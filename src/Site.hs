{-# LANGUAGE OverloadedStrings #-}
module Site where

import           Control.Applicative
import           Snap.Core
import           Snap.Snaplet
import           Snap.Snaplet.Heist
import           Snap.Util.FileServe
import           Snap.Http.Server
import           Heist
import qualified Data.ByteString as B

import Application

index :: Handler App App ()
index = do
  ifTop (render "index")

routes :: [(B.ByteString, Handler App App ())]
routes = [ ("/", index)
         , ("", with heist heistServe)
         , ("", serveDirectory "static")
         ]

app :: SnapletInit App App
app = makeSnaplet "OpenBoards" "A simple open source bulletin board." Nothing $ do
  h <- nestSnaplet "" heist $ heistInit "templates"
  addRoutes routes
  return $ App h
